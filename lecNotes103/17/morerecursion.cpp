#include<iostream>
using std::cin;
using std::cout;
using std::endl;
#include<time.h>
#include <cstdlib> // for atol
#include <map>
using std::map;
#include <vector>
using std::vector;

// write the base 10 digits of n vertically to cout
// if n = 2358, want to print:
// 2
// 3
// 5
// 8
void printVertically(size_t k)
{
	/* remember the steps:
	 * 1. solve base case
	 * 2. break down problem in terms of itself:
	 *    "if I had a magic box that worked for
	 *    inputs of size n-1, how could I use it
	 *    to solve my input of size n??"
	 * NOTE: here we will consider the *number of digits*
	 * as the "size" of the input, rather than the number itself.
	 * */
	if (k < 10) { /* only one digit */
		cout << k << endl;
		return;
	}
	/* now assume inductive hypothesis: this very function
	 * (which we're not even done writing yet!) works for
	 * any input with fewer digits than k.  Then we can use
	 * it to print all of k as follows: */
	/* first, print k/10, which necessarily has fewer digits! */
	printVertically(k/10);
	/* now just print the last digit: */
	cout << k%10 << endl;
	// printVertically(k%10 << endl; /* this would also work. */
}
/* TODO: make sure you can write this on your own and that you
 * understand why it works (trace the sequence of calls) */

/* TODO: write code to compute the greatest common divisor of two integers
 * Do this with recursion -- no loops!  */
unsigned long gcd(unsigned long a, unsigned long b)
{
	return 1;
}

/* TODO: write the *extended* GCD algorithm, which returns gcd(a,b), but
 * also sets u and v such that ua + vb = gcd(a,b) Warning: this might take a
 * little bit of thinking (if you don't just look up the answer online).  Save
 * it for last. */
unsigned long gcdEx(unsigned long a, unsigned long b, long& u, long& v)
{
	return 0;
}

/* A few functions to test out what we have done: */

void basicTest()
{
	printVertically(235819);
}

void gcdTest()
{
	// TODO: modify this to test your extended gcd algorithm,
	// once you've written it, that is.
	unsigned long a,b,d;
	while(true) {
		cout << "Enter a,b: ";
		cin >> a;
		cin >> b;
		if(a==0 || b==0) return;
		d = gcd(a,b);
		cout << "The gcd is: " << d << endl;
	}
}

int main(int argc, char** argv)
{
	basicTest();
	// gcdTest();
	return 0;
}
