// Introducing vectors; more on functions.

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector> /* <--- new stuff */
using std::vector;
#include <cstdio>

/* exercise: write a function that takes a vector and searches
 * for a particular value, returning true iff it is found. */
bool search(vector<int> v, int x) {
	for (size_t i = 0; i < v.size(); i++) {
		if (v[i] == x) return true;
	}
	return false;
}
/* NOTE: the above passes v *by value*.  Why might this be bad?
 * Could be expensive to copy the input into v if the vector has
 * many elements.  How to fix?  Could pass by reference, but then
 * we might accidentally change v.  Here's the fix: use *const*
 * by reference to sort of "fake" a by value call: */

bool search_better(const vector<int>& v, int x);
/* the const creates a contract with the compiler that v won't
 * be modified by this function.  The by reference (&) makes
 * the call efficient. */
/* TODO: fill out search_better (try to do it without looking at search).
 * Then change the function so that it would modify part of v (you could
 * add a push_back call, for example).  Make sure the subsequent compiler
 * errors make sense.  Be sure to test your code from main()! */

/* NOTE: the capacity() function tells us how much space the vector
 * has available.  Call this function to watch how it grows: */
void test() {
	/* watch how the vector grows: */
	printf("testing capacity growth...\n");
	vector<int> v;
	for (size_t i = 0; i < 20; i++) {
		printf("%lu\n",v.capacity());
		v.push_back(i);
	}
	/* TODO: figure out the pattern, and then see
	 * if you can figure out why they chose to use
	 * that pattern... */
}

int main()
{
	/* exercise: read list of strings, print in reverse order. */
	/* NOTE: the difficulty is that we don't know how many variables
	 * we will need ahead of time.  We need a way to allocate new
	 * variables as the program is running. */
	/* NOTE: vector itself isn't a datatype.  It is like a machine
	 * that creates datatypes: */
	// vector v; /* won't compile -- vector isn't a type */
	vector<string> v; /* NOTE: v will start out empty. */
	string s;
	while (cin >> s) {
		/* store s in the vector: */
		v.push_back(s);
	}
	/* we've now stored all of stdin!  Just need to print it
	 * in reverse order.  First, need to know how many we have: */
	cout << "Read " << v.size() << " strings.\n";
	for (int i = v.size()-1; i != -1; i--) {
		cout << v[i] << "\n";
	}
	/* TODO: figure out what is wrong with this version: */
	// int count = 0;
	// while(cin >> s) {
	// 	v[count] = s;
	// 	count++;
	// }

	return 0;
}

/* TODO: write a *binary search* on a sorted vector.  The idea is to
 * kind of emulate the process you use to find a particular page in a book:
 * 1. open the book to some page in the middle.
 * 2. if the page number was too high, open to the middle of the pages to the
 *    left; if it was too low, open to the middle of the pages to the right
 * 3. continue as above until you found the right page.
 *
 * This might be a little challenging.  Ask questions if you get stuck.
 * */

/* TODO: write a function that takes a vector and places the elements
 * in sorted order.  This is also kind of challenging. */

