#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <set> /* <-- new stuff. */
using std::set;
#include <string>
using std::string;
#include <cstdio>

void setTest() {
	/* store perfect squares in a set and then do some searches. */
	set<int> S;
	for (size_t i = 0; i < 10; i++) {
		S.insert(i*i);
	}
	int n;
	while (cin >> n) {
		if (S.find(n) != S.end())
			cout << n << " was a square < 100.\n";
		else
			cout << n << " was not a square < 100.\n";
	}
	/* How to go through the contents of a set? */
	cout << "set contents:\n\n";
	for(set<int>::iterator i = S.begin(); i != S.end(); i++) {
		/* what is the value?  *i, just like a pointer */
		cout << *i << " ";
	}
	cout << endl;
	/* NOTE: this is much like going though an array or vector: */
	#if 0
	int A[10];
	for (size_t i = 0; i < 10; i++) {
		A[i] = i*i*i;
	}
	// for(int* i = &A[0]; i != &A[10]; i++)
	for(int* i = A; i != (A+10); i++) {
		// cout << i << " "; // <-- this would print the addresses.
		cout << *i << " ";
	}
	cout << endl;
	#endif
	/* TODO: go through the example above with the vector and make sure
	 * you understand it! */
}

/* exercise: compute the intersection of two sets:
 * recall that the intersection of two sets is the set
 * consisting of the elements they have in common.  E.g.,
 * intersection of {2,3,4,5,6,7} and {1,2,4,7,11,44} is
 * {2,4,7}
 * */

set<int> intersect(const set<int>& S1, const set<int>& S2) {
	set<int> r;
	set<int>::iterator i;
	for(i = S1.begin(); i!= S1.end(); i++){
		/* if *i is also in S2, add it to the intersection */
		if(S2.find(*i) != S2.end()) r.insert(*i);
	}
	return r;
	/* TODO: the above isn't symmetric -- we iterate through S1,
	 * and search in S2. Does this matter?  Could we get away with
	 * less work somehow by looking at the sizes of S1 and S2?
	 * Modify the above accordingly. */
	/* TODO: I claim that for vectors, it wouldn't matter which
	 * one we loop though and which one we search in.  Make sure
	 * you understand why. */
}

/* TODO: write a function that returns the union of two sets */

/* TODO: emulate the insert function for the set, but for a vector.
 * That is, write a function that takes a vector (say of integers)
 * and a single integer x, and adds x to the vector *only if it was
 * not already present*. You can return a boolean indicating whether
 * or not x insertion took place. */

/* TODO: write a function that removes a value from a vector if it
 * is present.  It should take a vector and a value x, and remove
 * x if it is in the vector. NOTE: you don't have to preserve the
 * order of the other elements! */

void intTest() {
	set<int> s1 = {2,3,4,5,6,7};
	set<int> s2 = {1,2,4,7,11,44};
	set<int> s3 = intersect(s1,s2);
	cout << "intersection contained: ";
	for (set<int>::iterator i = s3.begin(); i != s3.end(); i++) {
		cout << *i << " ";
	}
	cout << endl;
}

/* NOTE: sets do not store duplicates: */

int main(void)
{
	intTest();
	// setTest();
	return 0;
}
