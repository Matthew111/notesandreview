#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <set>
using std::set;

/* TODO: write a function to compute all *k-subsets* of a given set.
 * Recall that k-subsets are subsets with *precisely* k elements.
 * Below are some function prototypes you could use.  Hint: you can
 * use a similar technique to the powerset, where you pick an element
 * to exclude and then make some recursive calls.  It is trickier,
 * but you can do it! */

/* with sets: */
set<set<int> > ksubsets(set<int>& S, size_t k);
/* or with vectors: */
vector<vector<int> > ksubsets(vector<int>& V, size_t k);

int main()
{
	/* TODO: write some test code. */
	return 0;
}
