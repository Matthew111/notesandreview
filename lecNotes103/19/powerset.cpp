#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;
#include <set>
using std::set;

typedef set<set<int> >::iterator ss_iter;
typedef set<int>::iterator s_iter;

set<set<int> > powerSet(set<int>& S)
{
	/* base case: S is empty: */
	if (S.size() == 0) {
		/* have to return a set containing the empty set. */
		set<set<int> > P; /* this will be the return value. */
		set<int> empty; /* sets will always be initialized to
						   the empty set, unless you say otherwise. */
		P.insert(empty);
		return P;
	}
	/* now we'll use recursive magic to compute the powerset of
	 * something smaller -- in particular, S \ {<first element>} */
	int first = *(S.begin());
	/* now remove it from S: */
	S.erase(S.begin()); /* you should be able to remove by value also... */
	/* now S is S' from the notes. */
	set<set<int> > PS_prime = powerSet(S); /* NOTE: this is ok because S
										     is now smaller by one element. */
	/* let's save a copy of powerset of S' */
	set<set<int> > P(PS_prime); /* NOTE: this invokes the "copy constructor".
								   P will be the return value. */
	/* at this moment, P contains all the subsets which don't have the
	 * first element.  All that's left: add copies which have the first
	 * element added. */
	for (ss_iter i = PS_prime.begin(); i != PS_prime.end() ; i++) {
		/* each *i is a set.  We want to add first to each one,
		 * and then add that whole set to P. */
		set<int> T(*i); /* make a copy of *i */
		/* add first: */
		T.insert(first);
		P.insert(T);
	}
	/* if you want, we can restore S: */
	S.insert(first);
	return P;
}

/* TODO: trace the above on small-ish input (say a 3 or 4 element set).  Make
 * sure you understand every detail. */
/* TODO: rewrite the above using vectors instead of sets.  (Hint: it might be
 * easier to exclude the last element rather than the first.) */

int main()
{
	/* run a test: */
	set<int> S = {1,2,3};
	set<set<int> > P = powerSet(S);
	for (ss_iter i = P.begin(); i != P.end(); i++) {
		cout << "{ ";
		for (s_iter j = i->begin(); j != i->end(); j++) {
			cout << *j << " ";
		}
		cout << "}\n";
	}
	return 0;
}

/* TODO: try to finish the extended gcd algorithm from the notes. */
