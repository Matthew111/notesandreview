/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
	/* Collatz conjecture: for all integers n, the following process
	 * will terminate in a finite number of steps:
	 *    if n == 1, stop;
	 *    if n is even, divide by 2;
	 *    if n is odd, multiply by 3 and add 1;
	 *    repeat using this new value of n.
	 * */
	/* let's test the conjecture for many integers, provided on
	 * standard input. */

	int n; /* holds input + intermediate computations */
	int noriginal; /* original value of n */
	int count; /* number of steps taken for n */
	while (cin >> n) {
		count = 0;
		noriginal = n;
		/* now test the conjecture for n. */
		while (n != 1) {
			count++;
			if (n % 2 == 0)
				n /= 2; /* same as n = n/2 */
			else
				n = n*3+1;
		}
		/* if we make it here, n satisfied the conjecture. */
		cout << noriginal << " stopped after " << count << " steps.\n";
	}

	return 0;
}

/* TODO: delete the entire thing and rewrite it from scratch. */
/* TODO: go through the bash-history file and see if you can make
 * sense out of it. */

// vim:foldlevel=1
