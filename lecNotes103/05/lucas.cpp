/* Lucas sequences. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
	/* Lucas sequences (of the first kind):
	 * U_n(P,Q) = P*U_{n-1}(P,Q) - Q*U_{n-2}(P,Q).
	 * The first two terms are defined as 0 and 1, respectively.
	 * */
	/* TODO: write this.  See the notes for an outline.  Make sure your
	 * code works for small values of n (0 and 1 in particular). */

	return 0;
}

// vim:foldlevel=1
