#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <set>
using namespace std;

/* TODO: write a function to compute all *k-subsets* of a given set.
 * Recall that k-subsets are subsets with *precisely* k elements.
 * Below are some function prototypes you could use.  Hint: you can
 * use a similar technique to the powerset, where you pick an element
 * to exclude and then make some recursive calls.  It is trickier,
 * but you can do it! */

/* TODO: finish the trace of our ksubset algorithm from lecture. */

/* TODO: Write a function that computes (or even just prints) all
 * permutations of a vector.  NOTE: it will be hard to use sets for
 * this, since the order is important!  So use vectors. */

/* with sets: */
set<set<int> > ksubsets(set<int>& S, size_t k){
	set<set<int> > R; set<int> empty;
	if(S.size() == 0){
		R.insert(empty); return R;
	}else{
		int last =*(S.begin());
		S.erase(S.begin());
		set<set<int> > S2 = ksubsets( S, k);
		for(set<set<int> >::iterator i=S2.begin(); i!=S2.end();i++){
			
		}
		R.insert( empty.insert(last) );
	}

};
/* or with vectors: */
vector<vector<int> > ksubsets(vector<int>& V, size_t k);

int main()
{
	/* TODO: write some test code. */
	return 0;
}
