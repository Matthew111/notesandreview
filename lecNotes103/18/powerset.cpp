#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;
#include <set>
using std::set;

/* TODO: try to write the powerset function from lecture.
 * NOTE: this is not that easy.  Below are a few prototypes that
 * you might consider using. */

/* with sets: */
set<set<int> > powerSet(set<int>& S);
/* or vectors could also work: */
vector<vector<int> > powerSet(vector<int>& V);

int main()
{
	/* TODO: test code goes here */
	return 0;
}

/* TODO: try to finish the extended gcd algorithm from the notes. */
