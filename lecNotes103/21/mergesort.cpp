#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;

/* TODO: try to reproduce this entire thing on your own. */

/* merge two sorted arrays into one */
void merge(vector<int>& A, size_t low, size_t mid, size_t high)
{
	/* our job here: given that A[low..mid] and A[mid+1..high]
	 * are sorted, fill A[low..high] with their contents in
	 * sorted order.  IDEA: keep in mind that thing with the
	 * stacks of cards... */
	/* these will keep track of the next value to be placed
	 * into the sorted vector from each subarray. */
	size_t lefttop = low;
	size_t righttop = mid+1;
	vector<int> sorted;
	while (lefttop <= mid && righttop <= high) {
		/* over and over, compare A[lefttop] with A[righttop] and
		 * place the smallest one at the back of another vector.
		 * Then make sure the meaning of {left,right}top is preserved. */
		if (A[lefttop] < A[righttop]) {
			sorted.push_back(A[lefttop++]);
		} else {
			sorted.push_back(A[righttop++]);
		}
	}
	while (lefttop <= mid) sorted.push_back(A[lefttop++]);
	while (righttop <= high) sorted.push_back(A[righttop++]);
	/* now the desired result is in sorted.  Copy it back to the
	 * right part of A: */
	for (size_t i = 0; i < sorted.size(); i++) A[i+low] = sorted[i];
}

/* sort A[low...high], inclusive. */
void mergeSort(vector<int>& A, size_t low, size_t high)
{
	/* handle base case: array is really small: */
	if (low >= high) return;
	/* compute mid point, recursively sort subarrays,
	 * then merge them back together. */
	size_t mid = (low+high) / 2;
	/* now recursively sort the subarrays: */
	mergeSort(A,low,mid);
	mergeSort(A,mid+1,high);
	/* TODO: convince yourself (via mathematical proof) that
	 * both of these recursive calls will be on non-empty,
	 * arrays (which are also disjoint).  Thus, we are guaranteed
	 * to hit the base case at some point. */
	/* now merge them back together */
	merge(A,low,mid,high);
}

int main(void)
{
	vector<int> A;
	int temp;
	while (cin >> temp) {
		A.push_back(temp);
	}
	mergeSort(A,0,A.size()-1);
	for (size_t i = 0; i < A.size(); i++) {
		cout << A[i] << " ";
	}
	cout << "\n";
	return 0;
}

/* TODO: test this out.  A not very interesting test could look like:
 * $ echo {10..1} | ./test
 * */
