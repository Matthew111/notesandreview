/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
	/* exercise: brute force gcd.  Compute the greatest common
	 * divisor of n and k by an exhaustive search.  */
	/* IDEA: start with the largest possible value that the gcd
	 * could have, then walk *backwards* until you find the first
	 * value that divides both inputs. */
	unsigned int a,b; /* inputs to gcd */
	cout << "enter two integers for gcd: ";
	cin >> a >> b;
	unsigned int d; /* NOTE: we choose unsigned ints so we don't have
							 to worry about negative numbers... */
	/* ensure a is the smallest one: */
	if (a > b) {
		/* swap contents of a and b */
		unsigned int temp = a; /* save value of a so we don't overwrite it */
		a = b;
		b = temp;
	}
	/* at this point a is the smaller one.  now we can follow our notes... */
	d = a;
	while (a%d != 0 || b%d != 0) {
		/* NOTE: the condition above basically says "d is not yet
		 * the gcd".  So, the whole thing can be summarized as
		 * "while there's something wrong with d, subtract one
		 * from d." */
		d--; /* shorthand for d = d - 1; */
	}
	cout << "gcd is " << d << "\n";
	/* TODO: shorten the above code.  We don't actually need to
	 * swap the contents of a and b.  Just set d to be the
	 * smaller of the two.  */

	/* exercise: brute force test for perfect cubes.  Check if
	 * n = k^3 for some integer k.  */
	unsigned int n;
	cin >> n;
	/* IDEA: start at 1, check if 1^3 == a, and if not, try
	 * the next largest integer... */
	unsigned int i;
	for (i = 1; i < n; i++) {
		/* check if i^3 == n */
		if (i*i*i == n) {
			break; /* this stops the innermost loop. */
		}
	}
	/* now just figure out *why* the loop ended... */
	if (i < n)
		cout << n << " was a cube.\n";
	else
		cout << n << " wasn't a cube.\n";
	/* TODO: make this also work for n == 1. */

	/* TODO: if you haven't done this already, read an arbitrary list of
	 * integers and then print the max, min, and mean.  Try to take the
	 * following very high level steps:
	 * 1. First, think about how many variables you will need, and for what
	 *    purposes.
	 * 2. Declare the variables, and in comments, state the meaning of each
	 *    variable.
	 * 3. Loop through input integers (using the method suggested for
	 *    project 2) and make sure the meaning of each of your variables
	 *    is preserved at the end of each iteration of the loop.
	 * 4. Finally, print the results.
	 * */


	/* TODO: write a loop that prints the sum of the first n odd cubes. */

	/* TODO: write code that gets an integer n from the user and prints out
	 * the n-th term of the fibonacci sequence. */

	/* TODO: a slight generalization of an earlier exercise: for integers
	 * n and k, determine the largest power of k that divides n. */

	return 0;
}

// vim:foldlevel=1
