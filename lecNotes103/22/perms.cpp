#include <iostream>
using std::cout;
using std::cin;
#include <vector>
using std::vector;

/* TODO: scrap this entire thing and write it on your own from scratch. */
/* TODO: try to explain it to a friend, or your cat, or an inanimate object
 * if none of the above seem to in the mood for conversation. */

vector<vector<int> > perms(vector<int> V)
{
	/* base case: one element: */
	if (V.size() == 1) return vector<vector<int> >(1,V);
	/* regarding the (1,V): the 1 is the size of the new
	 * vector, and each element will be a copy of V. */
	#if 0
	/* alternatively: */
	vector<vector<int> > R;
	R.push_back(V);
	return R;
	#endif
	/* conceptually: let each element "have a turn" at
	 * being last.  Then compute permutations of V by
	 * getting all permutations of V without it's last
	 * element, and then sticking it back on.  (For every
	 * choice of last element.) */
	vector<vector<int> > R;
	for (size_t i = 0; i < V.size(); i++) {
		/* let's say (in contrast with the notes), that
		 * V[i] will be last. */
		/* first, make it last: */
		int last = V[i];
		V[i] = V[V.size()-1];
		/* now exclude the last element, compute all permutations,
		 * then stick last element back on. */
		V.pop_back();
		vector<vector<int> > T = perms(V);
		/* NOTE: this isn't breaking any rules as V is now smaller! */
		/* what's in R? permutations of all of V without last.  what's
		 * left? add last back to each of these in the last place. */
		for (size_t j = 0; j < T.size(); j++) {
			T[j].push_back(last);
			R.push_back(T[j]);
		}
		/* now restore vector for next iteration: */
		V.push_back(V[i]);
		V[i] = last;
	}
	return R;
}

int main()
{
	/* TODO: test this out more.  'echo {1..4} | ./test' should suffice. */
	vector<int> V;
	int n;
	while (cin >> n) {
		V.push_back(n);
	}
	vector<vector<int> > R = perms(V);
	for (size_t i = 0; i < R.size(); i++) {
		for (size_t j = 0; j < R[i].size(); j++) {
			cout << R[i][j] << " ";
		}
		cout << "\n";
	}
	return 0;
}
