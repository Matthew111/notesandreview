/* Once again, some challenging vector exercises in case you missed them
 * the first time. */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <cstdio>
#include <algorithm>
using std::swap;

/* exercise: write a function that takes a vector and places the elements
 * in sorted order.  This is also kind of challenging. */
void sort(vector<int>& v) {
	/* IDEA: repeatedly find smallest element in
	 * v[i...size-1] and swap with v[i] */
	for (size_t i = 0; i < v.size() - 1; i++) {
		/* find smallest thing in v[i...size-1] */
		size_t indexOfSmallest = i;
		for (size_t j = i+1; j < v.size(); j++) {
			if (v[indexOfSmallest] > v[j]) {
				indexOfSmallest = j;
			}
		}
		swap(v[i],v[indexOfSmallest]);
	}
	/* NOTE: size_t is an integer datatype.  It will be the largest
	 * *unsigned* integer that fits in a machine register. */
}
/* TODO: scrutinize the above code *very* carefully.  Make sure you
 * know how it works.  Sit down with a patient friend, or your favorite
 * stuffed animal and try to explain every line to her/him.  If necessary,
 * pretend your friend is annoying, asks a lot of questions, and challenges
 * everything you say.
 * PS: In case you think I'm crazy, this is a real thing:
 * https://en.wikipedia.org/wiki/Rubber_duck_debugging
 * */

/* TODO: try to think of other ways to sort a vector.  Maybe even write
 * some code and try it out. */

int main()
{
	/* test our sorting function: */
	int x;
	vector<int> v;
	while (cin >> x) {
		v.push_back(x);
	}
	sort(v);
	for (size_t i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}
	cout << "\n";
	return 0;
}

/* TODO: write a *binary search* on a sorted vector.  The idea is to
 * kind of emulate the process you use to find a particular page in a book:
 * 1. open the book to some page in the middle.
 * 2. if the page number was too high, open to the middle of the pages to the
 *    left; if it was too low, open to the middle of the pages to the right
 * 3. continue as above until you found the right page.
 *
 * This might be a little challenging.  Ask questions if you get stuck.
 * */

