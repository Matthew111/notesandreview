#include <iostream>
using namespace std;

/* TODO: WITHOUT compiling and running this, see if you can predict
 * the output of f(4). */
void f(int n) {
	if (n == 0) {
		cout << 0 << " ";
		return;
	}
	cout<<n<<" ";
	f(n-1);
	/* TODO: test this out and make sure you understand the output.
	 * Also, swap the above lines (f(n-1) and the cout), and make
	 * sure that output makes sense as well */
}
/* TODO: write the factorial example from lecture. */
size_t factorial(int a){
	if(a==1){
		return 1;
	}else{
		return a*factorial(a-1);
	}
}
/* TODO: practice proving things by mathematical induction. */
/* TODO: write a recursive function to compute x^n (x to the n power)
 * where n is an integer. */
size_t pow(size_t a,size_t b){
	if(b==1){ return a;}else{return a*pow(a,b-1);};
}

int main(void)
{
	int w,x,y,z;
	cin>>w>>x>>y>>z;
	f(w);
	cout<<"\n";
	cout<<factorial(x);
	cout << "\n";
	cout<<pow(y,z);
	cout<<"\n";
	return 0;
}
