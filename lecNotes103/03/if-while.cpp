/* Flow of control:
 * Boolean expressions; if and while statements.
 * */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

int main()
{
	/* IF STATEMENTS */

	/* IDEA: execute a piece of code 0 or 1 times, conditioned upon a
	 * boolean expression.
	 * */

	cout << "Did you get enough sleep last night?\n";
	string answer;
	cin >> answer;
	if (answer == "yes") {
		cout << "yay! 8D\n";
	} else {
		cout << "too bad :\\\n";
	}

	/* general form:
	 * if (<boolean expression>) {
	 *     statements...
	 * } else if (<boolean expression>) {
	 *     more statements...
	 * } else {
	 *     statements if all other cases failed...
	 * }
	 * NOTE: exactly ONE of the blocks of statements will be excuted: that
	 * of the first "match" (true expression), or the else at the end.
	 * */

	/* NOTE: TWO equals signs (==) are needed to test equality.
	 * Otherwise you'll end up doing assignment. */
	int n;
	cout << "enter a number: ";
	cin >> n;
	if (n = 23) { /* XXX: only one equals sign is assignment! */
		cout << "looks like n was 23.\n";
	}
	/* what's going on?  Well, almost every expression has
	 * a datatype and a value, and assignment statements are
	 * no exception!  The value of an assignment statement
	 * is the value of the right hand side.  Lastly, note that
	 * ANY integer can be interpreted as a boolean using the rule
	 * 0 <===> false
	 * anything else <===> true. */
	/* TODO: make sure you understand the above example, as well as the
	 * warning message from the compiler. */
	/* TODO: write a similar example with "n = 0", which will never
	 * print the message, even if if n really was 0. */

	/* Boolean expressions: expressions that can evaluate to either
	 * true or false.  For example, x == 3, or response != "yes".
	 * other (binary) boolean operators:
	 * ==
	 * !=
	 * <
	 * >
	 * <=
	 * >=
	 * The above work on integers, strings, chars, and lots more.
	 * Furthermore, we have the following operators on booleans:
	 * && -- this gives the logical "and"
	 * || -- this gives the logical "or"
	 * !  -- this gives the logical negation
	 * */


	/* LOOPS */
	#if 0
	while (true) {
		cout << " 8D   8D   8D";
		/* watch out for infinite loops!  If you ever do this
		 * on accident (or on purpose), you can stop the madness
		 * in your terminal by hitting "ctrl-c".  */
	}
	#endif
	/* NOTE: the above #if 0 ... #endif is a nice way to comment out
	 * big portions of code while testing.  Just change the 0 to a 1 to
	 * put it back in the program. */

	/* general form:
	 * while (<boolean expression>) {
	 *    statements to do while expression is true...
	 * }
	 * */
	/* example: print a list of the first 10 cubes of integers.
	 * I.e., 1,8,27,... */
	int i = 1;
	while (i <= 10) {
		cout << i << "^3 == " << i*i*i << "\n";
		i++;
	}
	cout << "i == " << i << "\n"; /* TODO: make sure you know why
										   this prints 11. */

	/* TODO: given an integer n, find exponent of the largest power of two that
	 * divides n.  Example: if n = r*8 with r odd, then you should output 3
	 * since 8 = 2^3.  (You are just recovering the exponent of the 2 in the
	 * number's factorization into primes.)  We did this in the notes, but try
	 * to write it again here from scratch, WITHOUT LOOKING AT THE ANSWER!
	 * */
	/* IDEA: keep on dividing n by two until we can't,
	 * and keep track of how many times it worked. */

	/* TODO: write a piece of code that reads integers from standard
	 * input until a negative integer is entered, and then computes
	 * the minimal value, the maximal value, and the average value
	 * of all the (non-negative) integers that were entered.
	 * (Hint: you don't have to store all those numbers at once... )*/

	return 0;
}

/* TODO: compile this via the makefile instead of typing out the g++
 * commands yourself.  */

// vim:foldlevel=1
